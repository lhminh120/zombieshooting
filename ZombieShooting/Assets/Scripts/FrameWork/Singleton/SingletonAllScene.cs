﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonAllScene<T> : MonoBehaviour where T : UnityEngine.Component
{

    #region Fields
    private static bool _isAlive = true;
    private static T instance;

    #endregion

    #region Properties

    public static T Instance
    {
        get
        {
            if(!_isAlive) return null;
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
                if (instance == null)
                {
                    Debug.Log("null 3");
                    GameObject obj = new GameObject();
                    obj.name = typeof(T).Name;
                    instance = obj.AddComponent<T>();
                    DontDestroyOnLoad(obj);
                }
            }
            return instance;
        }
    }

    #endregion

    #region Methods

    public virtual void Awake()
    {
        if (instance == null)
        {
            instance = this as T;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (instance.GetHashCode() != this.GetHashCode())
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }

        }
    }

    public virtual void OnApplicationQuit()
    {
        if (instance != null)
        {
            instance = null;
            _isAlive = false;
        }
    }


    #endregion

}
