﻿
using System.Collections.Generic;

public abstract class Composite : Node
{
    public List<Node> childs;

    public void AddChild(Node child)
    {
        childs.Add(child);
    }
}
public class Sequence : Composite
{
    public override NodeState Run()
    {
        // no child -> then failed (sometime designer forgot to add Leaf nodes)
        if (childs.Count == 0) return NodeState.Failure;
        // evaluate childs
        for (int n = 0, amount = childs.Count; n < amount; n++)
        {
            var state = childs[n].Run();
            // break when reach any child that return Failure or Running
            if (state != NodeState.Success) return state;
        }
        // all child are Success, then return Success
        return NodeState.Success;
    }
}

public class Selector : Composite
{
    public override NodeState Run()
    {
        // no child -> then failed (sometime designer forgot to add Leaf node in tree)
        if (childs.Count == 0) return NodeState.Failure;
        // evaluate childs
        for (int n = 0, amount = childs.Count; n < amount; n++)
        {
            var state = childs[n].Run();
            // break when reach the first child with status = Sucess or Running (not Failure)
            if (state != NodeState.Failure) return state;
        }
        // no child success or running, then return failed
        return NodeState.Failure;
    }
}

public class Parallel : Composite
{
    private int _numS = 0;
    private int _numF = 0;
    public void SetNumberSuccess(int numS) => _numS = numS;
    public void SetNumberFailure(int numF) => _numF = numF;
    
    public override NodeState Run()
    {
        int tempNumS = 0, tempNumF = 0;
        // no child -> then failed (sometime designer forgot to add Leaf node in tree)
        if (childs.Count == 0) return NodeState.Failure;
        // evaluate childs
        for (int n = 0, amount = childs.Count; n < amount; n++)
        {
            var state = childs[n].Run();
            switch (state)
            {
                case NodeState.Success:
                    tempNumS++;
                    break;
                case NodeState.Failure:
                    tempNumF++;
                    break;
            }
        }
        if(_numS <= tempNumS && _numF >= tempNumF)
        {
            return NodeState.Success;
        }
        return NodeState.Failure;
    }
}
