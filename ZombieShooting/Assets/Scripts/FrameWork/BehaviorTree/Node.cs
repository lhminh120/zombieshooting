﻿
public abstract class Node
{
    public Node _parent;
    public abstract NodeState Run();
}
