﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private int _damageHead;
    [SerializeField] private int _damageBody;
    [SerializeField] private bool _isMachineGun;
    public void Equip()
    {
        if (_isMachineGun)
        {
            GameManager.Instance._playerController.SetMachineDamage(_damageHead, _damageBody);
        }
        else
        {
            GameManager.Instance._playerController.SetPistolDamage(_damageHead, _damageBody);
        }

    }
}
