﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundLibrary : MonoBehaviour
{
    [Header("Background Sound")]
    [SerializeField] private AudioClip[] _backgroundSounds;

    [Header("Effect Sound")]
    [SerializeField] private AudioClip _thud;
    [SerializeField] private AudioClip _click;
    [SerializeField] private AudioClip _back;
    [Header("Walk Sound Effect")]
    [SerializeField] private AudioClip _walking;
    [SerializeField] private AudioClip _running;

    public enum SoundEffectName
    {
        NONE,
        THUD,
        CLICK,
        BACK,
        WALKING,
    }
    public AudioClip GetBackgroundSoundByID(int id) => _backgroundSounds[id];
    public AudioClip GetEffectSoundByName(SoundEffectName soundEffectName)
    {
        switch (soundEffectName)
        {
            case SoundEffectName.THUD: return _thud;
            case SoundEffectName.CLICK: return _click;
            case SoundEffectName.BACK: return _back;
        }
        return null;
    }
}
