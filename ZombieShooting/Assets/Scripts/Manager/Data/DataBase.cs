﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBase : SingletonAllScene<DataBase>
{
    public override void Awake()
    {
        base.Awake();
        LoadData();
    }
    public void LoadData()
    {
        LoadSoundSetting();
        LoadEffectSetting();
        LoadVibrateSetting();
        LoadBullet();
    }
    public void SaveDataSetting()
    {
        SaveSoundSetting();
        SaveEffectSetting();
        SaveVibrateSetting();
        SaveBullet();
    }
    #region Sound and Music
    private Action<float> _callBackChangeBackgroundVolumn;
    private Action<float> _callBackChangeEffectSoundVolumn;
    public void InitActionOnChangeVolumn(Action<float> callBackChangeBackgroundVolumn = null, Action<float> callBackChangeEffectSoundVolumn = null)
    {
        _callBackChangeBackgroundVolumn += callBackChangeBackgroundVolumn;
        _callBackChangeEffectSoundVolumn += callBackChangeEffectSoundVolumn;
    }
    private float _backgroundSoundVolumn = 1;
    private float _soundEffectVolumn = 1;
    public float BackgroundSoundVolumn
    {
        get
        {
            return _backgroundSoundVolumn;
        }
        set
        {
            _backgroundSoundVolumn = value;
            _callBackChangeBackgroundVolumn?.Invoke(_backgroundSoundVolumn);
        }
    }
    public float EffectSoundVolumn
    {
        get
        {
            return _soundEffectVolumn;
        }
        set
        {
            _soundEffectVolumn = value;
            _callBackChangeEffectSoundVolumn?.Invoke(_soundEffectVolumn);
        }
    }
    private void LoadSoundSetting()
    {
        BackgroundSoundVolumn = PlayerPrefs.GetFloat(GlobalVariables.BACKGROUND_SOUND_SETTING_KEY, 1f);
        EffectSoundVolumn = PlayerPrefs.GetFloat(GlobalVariables.EFFECT_SOUND_SETTING_KEY, 1f);
    }
    private void SaveSoundSetting()
    {
        PlayerPrefs.SetFloat(GlobalVariables.BACKGROUND_SOUND_SETTING_KEY, BackgroundSoundVolumn);
        PlayerPrefs.SetFloat(GlobalVariables.EFFECT_SOUND_SETTING_KEY, EffectSoundVolumn);
    }
    #endregion

    #region Effect
    private bool _allowEffect = true;
    public bool AllowEffect
    {
        get => _allowEffect;
        set => _allowEffect = value;
    }
    private void LoadEffectSetting() => _allowEffect = PlayerPrefs.GetInt(GlobalVariables.EFFECT_SETTING_KEY, 0) == 1;
    private void SaveEffectSetting() => PlayerPrefs.SetInt(GlobalVariables.EFFECT_SETTING_KEY, _allowEffect ? 1 : 0);
    #endregion

    #region Vibrate
    private bool _allowVibrate = true;
    public bool AllowVibrate
    {
        get => _allowVibrate;
        set => _allowVibrate = value;
    }
    private void LoadVibrateSetting() => _allowVibrate = PlayerPrefs.GetInt(GlobalVariables.VIBRATE_SETTING_KEY, 0) == 1;
    private void SaveVibrateSetting() => PlayerPrefs.SetInt(GlobalVariables.VIBRATE_SETTING_KEY, _allowVibrate ? 1 : 0);
    #endregion
    #region Player Data
    public Action<int, int> _callBackOnUpdateAmmo;
    private int _totalMachineGunBulletsNotUsing = 100;
    private int _totalPistolGunBulletsNoUsing = 100;
    public int TotalMachineGunBullets
    {
        get => _totalMachineGunBulletsNotUsing;
        set
        {
            _totalMachineGunBulletsNotUsing = value;
            _callBackOnUpdateAmmo?.Invoke(_totalBulletsInMachineGun, _totalMachineGunBulletsNotUsing);
        }
    }
    public int TotalPistolGunBullets
    {
        get => _totalPistolGunBulletsNoUsing;
        set
        {
            _totalPistolGunBulletsNoUsing = value;
            _callBackOnUpdateAmmo?.Invoke(_totalBulletsInPistolGun, _totalPistolGunBulletsNoUsing);
        }
    }
    private int _totalBulletsInMachineGun = 0;
    private int _totalBulletsInPistolGun = 0;
    public int TotalBulletsInMachineGun
    {
        get => _totalBulletsInMachineGun;
        set
        {
            _totalBulletsInMachineGun = value;
            _callBackOnUpdateAmmo?.Invoke(_totalBulletsInMachineGun, _totalMachineGunBulletsNotUsing);
        }
    }
    public int TotalBulletsInPistolGun
    {
        get => _totalBulletsInPistolGun;
        set
        {
            _totalBulletsInPistolGun = value;
            _callBackOnUpdateAmmo?.Invoke(_totalBulletsInPistolGun, _totalPistolGunBulletsNoUsing);
        }
    }
    private void LoadBullet()
    {
        _totalBulletsInMachineGun = PlayerPrefs.GetInt(GlobalVariables.TOTAL_BULLETS_IN_MACHINE_GUN, 0);
        _totalBulletsInPistolGun = PlayerPrefs.GetInt(GlobalVariables.TOTAL_BULLETS_IN_PISTOL_GUN, 0);
        _totalMachineGunBulletsNotUsing = PlayerPrefs.GetInt(GlobalVariables.TOTAL_MACHINE_GUN_BULLETS_NOT_USING, 100);
        _totalPistolGunBulletsNoUsing = PlayerPrefs.GetInt(GlobalVariables.TOTAL_PISTOL_GUN_BULLETS_NOT_USING, 100);
    }
    private void SaveBullet()
    {
        PlayerPrefs.SetInt(GlobalVariables.TOTAL_BULLETS_IN_MACHINE_GUN, _totalBulletsInMachineGun);
        PlayerPrefs.SetInt(GlobalVariables.TOTAL_BULLETS_IN_PISTOL_GUN, _totalBulletsInPistolGun);
        PlayerPrefs.SetInt(GlobalVariables.TOTAL_MACHINE_GUN_BULLETS_NOT_USING, _totalMachineGunBulletsNotUsing);
        PlayerPrefs.SetInt(GlobalVariables.TOTAL_PISTOL_GUN_BULLETS_NOT_USING, _totalPistolGunBulletsNoUsing);
    }
    #endregion
}
