﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GamePlayUIView : MonoBehaviour
{
    [Header("Text")]
    [SerializeField] private TMP_Text _playerAmmoShowTxt;
    [SerializeField] private TMP_Text _playerWeaponUsingTxt;
    [SerializeField] private TMP_Text _playerHeartTxt;
    [Header("Image")]
    [SerializeField] private Image _bloodEffectImg;
    private float _countUpTimeBloodEffect = 0;
    private bool _isRunningBloodEffect = false;
    private float _bloodEffectDurationTime = 0.2f;
    public void SetPlayerAmmoShowTxt(int ammoInGun, int ammoNotUsing)
    {
        _playerAmmoShowTxt.text = ammoInGun + "/" + ammoNotUsing;
    }
    public void SetPlayerWeaponName(string weaponName)
    {
        _playerWeaponUsingTxt.text = weaponName;
    }
    public void SetPlayerHeart(int playerHeart)
    {
        if (playerHeart < 0) playerHeart = 0;
        _playerHeartTxt.text = "Player Heart " + playerHeart;
    }
    public void StartBloodEffect()
    {
        _isRunningBloodEffect = true;
        _countUpTimeBloodEffect = 0;
    }
    private void RunBloodEffect()
    {
        if (_isRunningBloodEffect)
        {
            if (_countUpTimeBloodEffect < _bloodEffectDurationTime)
            {
                _countUpTimeBloodEffect += Time.deltaTime;
                float process = _countUpTimeBloodEffect / _bloodEffectDurationTime / 2;
                if (process > 1) process = 1;
                var imgColor = _bloodEffectImg.color;
                imgColor.a = process;
                _bloodEffectImg.color = imgColor;
            }
            else
            {
                _isRunningBloodEffect = false;
                _countUpTimeBloodEffect = 0;
                var imgColor = _bloodEffectImg.color;
                imgColor.a = 0;
                _bloodEffectImg.color = imgColor;
            }
        }
    }
    private void Update()
    {
        RunBloodEffect();
    }
}
