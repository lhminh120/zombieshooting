using UnityEngine;
public static class GlobalVariables
{
    public static Vector3 _vecForward = Vector3.forward;
    public static Vector3 _vecUp = Vector3.up;
    public static Vector3 _vecZero = Vector3.zero;
    public static Vector3 _vecDown = Vector3.down;
    public static Vector3 _vecRight = Vector3.right;
    public static Vector3 _vecLeft = Vector3.left;
    public static Vector3 _verForward = Vector3.forward;
    public static Vector3 _vecBack = Vector3.back;
    public enum LeanEase
    {
        // Basic
        Linear,
        Smooth = 100,
        Accelerate = 200,
        Decelerate = 250,
        Elastic = 300,
        Back = 400,
        Bounce = 500,

        // Advanced
        SineIn = 1000,
        SineOut,
        SineInOut,

        QuadIn = 1100,
        QuadOut,
        QuadInOut,

        CubicIn = 1200,
        CubicOut,
        CubicInOut,

        QuartIn = 1300,
        QuartOut,
        QuartInOut,

        QuintIn = 1400,
        QuintOut,
        QuintInOut,

        ExpoIn = 1500,
        ExpoOut,
        ExpoInOut,

        CircIn = 1600,
        CircOut,
        CircInOut,

        BackIn = 1700,
        BackOut,
        BackInOut,

        ElasticIn = 1800,
        ElasticOut,
        ElasticInOut,

        BounceIn = 1900,
        BounceOut,
        BounceInOut,
    }
    public static string BACKGROUND_SOUND_SETTING_KEY = "BACKGROUND_SOUND_SETTING_KEY";
    public static string EFFECT_SOUND_SETTING_KEY = "EFFECT_SOUND_SETTING_KEY";
    public static string EFFECT_SETTING_KEY = "EFFECT_SETTING_KEY";
    public static string VIBRATE_SETTING_KEY = "VIBRATE_SETTING_KEY";
    public static string TOTAL_MACHINE_GUN_BULLETS_NOT_USING = "TOAL_MACHINE_GUN_BULLETS_NOT_USING";
    public static string TOTAL_PISTOL_GUN_BULLETS_NOT_USING = "TOTAL_PISTOL_GUN_BULLETS_NOT_USING";
    public static string TOTAL_BULLETS_IN_MACHINE_GUN = "TOTAL_BULLETS_IN_MACHINE_GUN";
    public static string TOTAL_BULLETS_IN_PISTOL_GUN = "TOTAL_BULLETS_IN_PISTOL_GUN";
}