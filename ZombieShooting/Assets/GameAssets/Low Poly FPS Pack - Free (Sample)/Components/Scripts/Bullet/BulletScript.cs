﻿using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;

// ----- Low Poly FPS Pack Free Version -----
public class BulletScript : MonoBehaviour
{

    [Range(1, 100)]
    [Tooltip("After how long time should the bullet prefab be destroyed?")]
    public float destroyAfter;
    [Tooltip("If enabled the bullet destroys on impact")]
    public bool destroyOnImpact = false;
    [Tooltip("Minimum time after impact that the bullet is destroyed")]
    public float minDestroyTime;
    [Tooltip("Maximum time after impact that the bullet is destroyed")]
    public float maxDestroyTime;

    private Transform _transform;
    private void Awake()
    {
        _transform = transform;
    }

    private void OnEnable()
    {
        //Start destroy timer
        StartCoroutine(DestroyAfter());
    }

    //If the bullet collides with anything
    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);

    }

    private IEnumerator DestroyTimer()
    {
        //Wait random time based on min and max values
        yield return new WaitForSeconds
            (Random.Range(minDestroyTime, maxDestroyTime));
        //Destroy bullet object
        gameObject.SetActive(false);
    }

    private IEnumerator DestroyAfter()
    {
        //Wait for set amount of time
        yield return new WaitForSeconds(destroyAfter);
        //Destroy bullet object
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }

    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
// ----- Low Poly FPS Pack Free Version -----