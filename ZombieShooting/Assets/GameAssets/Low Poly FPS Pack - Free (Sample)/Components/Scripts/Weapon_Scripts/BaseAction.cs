﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAction : MonoBehaviour
{
    [Header("Player Controller")]
    public PlayerController playerController;
    //Animator component attached to weapon
    protected Animator anim;

    [Header("Gun Camera")]
    //Main gun camera
    public Camera gunCamera;

    [Header("Gun Camera Options")]
    //How fast the camera field of view changes when aiming 
    [Tooltip("How fast the camera field of view changes when aiming.")]
    public float fovSpeed = 15.0f;
    //Default camera field of view
    [Tooltip("Default value for camera field of view (40 is recommended).")]
    public float defaultFov = 40.0f;

    public float aimFov = 25.0f;

    [Header("Weapon Sway")]
    //Enables weapon sway
    [Tooltip("Toggle weapon sway.")]
    public bool weaponSway;

    public float swayAmount = 0.02f;
    public float maxSwayAmount = 0.06f;
    public float swaySmoothValue = 4.0f;

    private Vector3 initialSwayPosition;

    //Used for fire rate
    protected float lastFired;
    [Header("Weapon Settings")]
    //How fast the weapon fires, higher value means faster rate of fire
    [Tooltip("How fast the weapon fires, higher value means faster rate of fire.")]
    public float fireRate;
    //Eanbles auto reloading when out of ammo
    [Tooltip("Enables auto reloading when out of ammo.")]
    public bool autoReload;
    //Delay between shooting last bullet and reloading
    public float autoReloadDelay;
    //Check if reloading
    protected bool isReloading;

    //Holstering weapon
    protected bool hasBeenHolstered = false;
    //If weapon is holstered
    protected bool holstered;
    //Check if running
    protected bool isRunning;
    //Check if aiming
    protected bool isAiming;
    //Check if walking
    protected bool isWalking;
    //Check if inspecting weapon
    protected bool isInspecting;
    protected bool isUsingKnife_1;
    protected bool isUsingKnife_2;
    protected bool isThrowingGernade;
    protected bool doneMakingKnife_1_Damage = false;
    protected bool doneMakingKnife_2_Damage = false;


    //Totalt amount of ammo
    [Tooltip("How much ammo the weapon should have.")]
    public int ammo;
    //Check if out of ammo
    protected bool outOfAmmo;

    [Header("Bullet Settings")]
    //Bullet
    [Tooltip("How much force is applied to the bullet when shooting.")]
    public float bulletForce = 400.0f;
    // [Tooltip("How much force is applied to the bullet when shooting.")]
    // public float bulletForce = 400.0f;
    [Tooltip("How long after reloading that the bullet model becomes visible " +
        "again, only used for out of ammo reload animations.")]
    public float showBulletInMagDelay = 0.6f;
    [Tooltip("The bullet model inside the mag, not used for all weapons.")]
    public SkinnedMeshRenderer bulletInMagRenderer;

    [Header("Grenade Settings")]
    public float grenadeSpawnDelay = 0.35f;
    [Header("Knife Setting")]
    public float knife_1_Delay = 0.5f;
    public float knife_2_Delay = 2f;

    [Header("Muzzleflash Settings")]
    public bool randomMuzzleflash = false;
    //min should always bee 1
    protected int minRandomValue = 1;

    [Range(2, 25)]
    public int maxRandomValue = 5;

    protected int randomMuzzleflashValue;

    public bool enableMuzzleflash = true;
    public ParticleSystem muzzleParticles;
    public bool enableSparks = true;
    public ParticleSystem sparkParticles;
    public int minSparkEmission = 1;
    public int maxSparkEmission = 7;

    [Header("Muzzleflash Light Settings")]
    public Light muzzleflashLight;
    public float lightDuration = 0.02f;

    [Header("Audio Source")]
    //Main audio source
    public AudioSource mainAudioSource;
    //Audio source used for shoot sound
    public AudioSource shootAudioSource;
    protected ObjectKey _bulletPrefabKey = ObjectKey.BULLET;
    protected ObjectKey _casingPrefabKey = ObjectKey.BIG_CASING;
    protected ObjectKey _grenadePrefabKey = ObjectKey.GRENADE;

    [System.Serializable]

    public class spawnpoints
    {
        [Header("Spawnpoints")]
        //Array holding casing spawn points 
        //(some weapons use more than one casing spawn)
        //Casing spawn point array
        public Transform casingSpawnPoint;
        //Bullet prefab spawn from this point
        public Transform bulletSpawnPoint;

        public Transform grenadeSpawnPoint;
    }
    public spawnpoints Spawnpoints;

    [System.Serializable]
    public class soundClips
    {
        public AudioClip shootSound;
        public AudioClip takeOutSound;
        public AudioClip holsterSound;
        public AudioClip reloadSoundOutOfAmmo;
        public AudioClip reloadSoundAmmoLeft;
        public AudioClip aimSound;
    }
    public soundClips SoundClips;

    protected bool soundHasPlayed = false;
    [Header("Shoot Setting")]
    [SerializeField] protected Transform _shootPoint;
    [SerializeField] protected float _shootRange = 100f;
    [SerializeField] protected LayerMask _layerMaskCanShoot;
    private int _damageHeadBullet = 10;
    private int _damageBodyBullet = 5;
    private int _knife_1_DamageHead = 50;
    private int _knife_1_DamageBody = 35;
    private int _knife_2_DamageHead = 20;
    private int _knife_2_DamageBody = 10;
    private int _gernadeDamage = 40;
    [Header("Impact Effect")]
    [SerializeField] private GameObject _impactEffectBullet;

    protected void Awake()
    {

        //Set the animator component
        anim = GetComponent<Animator>();
        muzzleflashLight.enabled = false;
    }

    protected void Start()
    {

        //Weapon sway
        initialSwayPosition = transform.localPosition;

        //Set the shoot sound to audio source
        shootAudioSource.clip = SoundClips.shootSound;
    }

    protected void LateUpdate()
    {

        //Weapon sway
        if (weaponSway == true)
        {
            float movementX = -Input.GetAxis("Mouse X") * swayAmount;
            float movementY = -Input.GetAxis("Mouse Y") * swayAmount;
            //Clamp movement to min and max values
            movementX = Mathf.Clamp
                (movementX, -maxSwayAmount, maxSwayAmount);
            movementY = Mathf.Clamp
                (movementY, -maxSwayAmount, maxSwayAmount);
            //Lerp local pos
            Vector3 finalSwayPosition = new Vector3
                (movementX, movementY, 0);
            transform.localPosition = Vector3.Lerp
                (transform.localPosition, finalSwayPosition +
                    initialSwayPosition, Time.deltaTime * swaySmoothValue);
        }
    }

    protected void Update()
    {
        if (playerController._isChangeGun) return;
        //Aiming
        //Toggle camera FOV when right click is held down
        if (Input.GetButton("Fire2") && !isReloading && !isRunning && !isInspecting)
        {

            isAiming = true;
            //Start aiming
            anim.SetBool("Aim", true);

            //When right click is released
            gunCamera.fieldOfView = Mathf.Lerp(gunCamera.fieldOfView,
                aimFov, fovSpeed * Time.deltaTime);

            if (!soundHasPlayed)
            {
                mainAudioSource.clip = SoundClips.aimSound;
                mainAudioSource.Play();

                soundHasPlayed = true;
            }
        }
        else
        {
            //When right click is released
            gunCamera.fieldOfView = Mathf.Lerp(gunCamera.fieldOfView,
                defaultFov, fovSpeed * Time.deltaTime);

            isAiming = false;
            //Stop aiming
            anim.SetBool("Aim", false);

            soundHasPlayed = false;
        }
        //Aiming end

        //If randomize muzzleflash is true, genereate random int values
        if (randomMuzzleflash == true)
        {
            randomMuzzleflashValue = Random.Range(minRandomValue, maxRandomValue);
        }

        //Continosuly check which animation 
        //is currently playing
        AnimationCheck();

        //Play knife attack 1 animation when Q key is pressed
        if (Input.GetKeyDown(KeyCode.Q) && !isInspecting && !isUsingKnife_1 && !isUsingKnife_2)
        {
            StartCoroutine(KnifeAnimDelay_1());
        }
        //Play knife attack 2 animation when F key is pressed
        if (Input.GetKeyDown(KeyCode.F) && !isInspecting && !isUsingKnife_1 && !isUsingKnife_2)
        {
            StartCoroutine(KnifeAnimDelay_2());
        }

        //Throw grenade when pressing G key
        if (Input.GetKeyDown(KeyCode.G) && !isInspecting && !isThrowingGernade)
        {
            StartCoroutine(GrenadeSpawnDelay());
            //Play grenade throw animation
            anim.Play("GrenadeThrow", 0, 0.0f);
        }

        //If out of ammo
        if (GetCurrentAmmo() == 0)
        {
            //Toggle bool
            outOfAmmo = true;
            //Auto reload if true
            if (autoReload == true && !isReloading)
            {
                StartCoroutine(AutoReload());
            }
        }
        else
        {
            //Toggle bool
            outOfAmmo = false;
            //anim.SetBool ("Out Of Ammo", false);
        }

        //AUtomatic fire
        //Left click hold 
        ShootBullet();

        //Inspect weapon when T key is pressed
        if (Input.GetKeyDown(KeyCode.T))
        {
            anim.SetTrigger("Inspect");
        }



        //Reload 
        if (Input.GetKeyDown(KeyCode.R) && !isReloading && !isInspecting)
        {
            //Reload
            Reload();
        }

        //Walking when pressing down WASD keys
        if (Input.GetKey(KeyCode.W) && !isRunning ||
            Input.GetKey(KeyCode.A) && !isRunning ||
            Input.GetKey(KeyCode.S) && !isRunning ||
            Input.GetKey(KeyCode.D) && !isRunning)
        {
            anim.SetBool("Walk", true);
        }
        else
        {
            anim.SetBool("Walk", false);
        }

        //Running when pressing down W and Left Shift key
        if ((Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)))
        {
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }

        //Run anim toggle
        if (isRunning == true)
        {
            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }
    }
    protected abstract void ShootBullet();
    public void HolsteredWeapon(bool hasBeenHolstered)
    {
        //Toggle weapon holster when E key is pressed
        if (!hasBeenHolstered)
        {
            holstered = true;

            mainAudioSource.clip = SoundClips.holsterSound;
            mainAudioSource.Play();

            hasBeenHolstered = true;
        }
        else if (hasBeenHolstered)
        {
            holstered = false;

            mainAudioSource.clip = SoundClips.takeOutSound;
            mainAudioSource.Play();

            hasBeenHolstered = false;
        }
        //Holster anim toggle
        if (holstered == true)
        {
            anim.SetBool("Holster", true);
        }
        else
        {
            anim.SetBool("Holster", false);
        }
    }

    protected IEnumerator GrenadeSpawnDelay()
    {

        //Wait for set amount of time before spawning grenade
        isThrowingGernade = true;
        yield return new WaitForSeconds(grenadeSpawnDelay);
        isThrowingGernade = false;
        //Spawn grenade prefab at spawnpoint
        var gernadePrefab = ObjectPool.Instance.SpawnObject(_grenadePrefabKey);
        gernadePrefab.SetActive(true);
        gernadePrefab.transform.SetPositionAndRotation(Spawnpoints.grenadeSpawnPoint.transform.position,
                                                        Spawnpoints.grenadeSpawnPoint.transform.rotation);
    }
    protected IEnumerator KnifeAnimDelay_1()
    {
        isUsingKnife_1 = true;
        anim.Play("Knife Attack 1", 0, 0f);
        yield return new WaitForSeconds(knife_1_Delay);
        isUsingKnife_1 = false;

    }
    protected IEnumerator KnifeAnimDelay_2()
    {
        isUsingKnife_2 = true;
        anim.Play("Knife Attack 2", 0, 0f);
        yield return new WaitForSeconds(knife_2_Delay);
        isUsingKnife_2 = false;

    }

    protected IEnumerator AutoReload()
    {
        //Wait set amount of time
        yield return new WaitForSeconds(autoReloadDelay);

        Reload();
    }

    //Reload
    protected virtual void Reload()
    {
        if (!GetTheBulletsReload()) return;
        if (outOfAmmo)
        {
            //Play diff anim if out of ammo
            anim.Play("Reload Out Of Ammo", 0, 0f);

            mainAudioSource.clip = SoundClips.reloadSoundOutOfAmmo;
            mainAudioSource.Play();

            //If out of ammo, hide the bullet renderer in the mag
            //Do not show if bullet renderer is not assigned in inspector
            if (bulletInMagRenderer != null)
            {
                bulletInMagRenderer.GetComponent
                <SkinnedMeshRenderer>().enabled = false;
                //Start show bullet delay
                StartCoroutine(ShowBulletInMag());
            }
            outOfAmmo = false;
        }
        else
        {
            //Play diff anim if ammo left
            anim.Play("Reload Ammo Left", 0, 0f);

            mainAudioSource.clip = SoundClips.reloadSoundAmmoLeft;
            mainAudioSource.Play();

            //If reloading when ammo left, show bullet in mag
            //Do not show if bullet renderer is not assigned in inspector
            if (bulletInMagRenderer != null)
            {
                bulletInMagRenderer.GetComponent
                <SkinnedMeshRenderer>().enabled = true;
            }
            outOfAmmo = false;
        }

    }

    //Enable bullet in mag renderer after set amount of time
    protected IEnumerator ShowBulletInMag()
    {

        //Wait set amount of time before showing bullet in mag
        yield return new WaitForSeconds(showBulletInMagDelay);
        bulletInMagRenderer.GetComponent<SkinnedMeshRenderer>().enabled = true;
    }

    //Show light when shooting, then disable after set amount of time
    protected IEnumerator MuzzleFlashLight()
    {

        muzzleflashLight.enabled = true;
        yield return new WaitForSeconds(lightDuration);
        muzzleflashLight.enabled = false;
    }

    //Check current animation playing
    protected void AnimationCheck()
    {

        //Check if reloading
        //Check both animations
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Reload Out Of Ammo") ||
            anim.GetCurrentAnimatorStateInfo(0).IsName("Reload Ammo Left"))
        {
            isReloading = true;
        }
        else
        {
            isReloading = false;
        }

        //Check if inspecting weapon
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Inspect"))
        {
            isInspecting = true;
        }
        else
        {
            isInspecting = false;
        }
    }
    //How much ammo is currently left
    protected abstract int GetCurrentAmmo();
    protected abstract void SetCurrentAmmo(int ammo);
    protected abstract int GetTotalBulletNotUsing();
    protected abstract void SetTotalBulletNotUsing(int bullet);
    protected bool GetTheBulletsReload()
    {
        int bulletsNeedToAdd = ammo - GetCurrentAmmo();
        if (bulletsNeedToAdd <= GetTotalBulletNotUsing())
        {
            SetTotalBulletNotUsing(GetTotalBulletNotUsing() - bulletsNeedToAdd);
            SetCurrentAmmo(ammo);
            return true;
        }
        else
        {
            if (GetTotalBulletNotUsing() > 0)
            {
                SetCurrentAmmo(GetCurrentAmmo() + GetTotalBulletNotUsing());
                SetTotalBulletNotUsing(0);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    protected void ChechTarget()
    {
        RaycastHit hit;
        if (Physics.Raycast(_shootPoint.position, _shootPoint.forward, out hit, _shootRange, _layerMaskCanShoot))
        {
            if (hit.transform.tag == "Head")
            {
                hit.transform.GetComponent<ZombiePart>().TakeDamage(_damageHeadBullet);
            }
            else if (hit.transform.tag == "Body")
            {
                hit.transform.GetComponent<ZombiePart>().TakeDamage(_damageBodyBullet);
            }
            Instantiate(_impactEffectBullet, hit.point, Quaternion.identity);
        }

    }
    public void SetGunDamage(int damageHead, int damageBody)
    {
        _damageHeadBullet = damageHead;
        _damageBodyBullet = damageBody;
    }
    public void SetKnifeDamage(int knife_1_DamageHead, int knife_1_DamageBody, int knife_2_DamageHead, int knife_2_DamageBody)
    {
        _knife_1_DamageHead = knife_1_DamageHead;
        _knife_1_DamageBody = knife_1_DamageBody;
        _knife_2_DamageHead = knife_2_DamageHead;
        _knife_2_DamageBody = knife_2_DamageBody;
    }
    public void SetGernadeDamage(int gernadeDamage)
    {
        _gernadeDamage = gernadeDamage;
    }
    private void OnTriggerStay(Collider other)
    {

        if (isUsingKnife_1)
        {
            Knife_1_MakeDamage(other);
        }
        else if (isUsingKnife_2)
        {
            Knife_2_MakeDamage(other);
        }

    }
    public void Knife_1_MakeDamage(Collider other)
    {
        if (other.tag == "Head")
        {
            other.GetComponent<ZombiePart>().TakeDamage(_knife_1_DamageHead);
        }
        else if (other.tag == "Body")
        {
            other.GetComponent<ZombiePart>().TakeDamage(_knife_1_DamageBody);
        }
    }
    public void Knife_2_MakeDamage(Collider other)
    {
        if (other.tag == "Head")
        {
            other.GetComponent<ZombiePart>().TakeDamage(_knife_2_DamageHead);
        }
        else if (other.tag == "Body")
        {
            other.GetComponent<ZombiePart>().TakeDamage(_knife_2_DamageBody);
        }
    }
}

